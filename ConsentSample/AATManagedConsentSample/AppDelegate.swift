//
//  AppDelegate.swift
//  AATManagedConsentSample
//
//  Created by Mahmoud Amer on 04.06.21.
//

import UIKit
import AATKit
import AppTrackingTransparency
import AATGoogleCMPAdapter
import AATSourcePointCMPAdapter

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK:- Properties
    var cmp = AATCMPGoogle()
//    let cmp = AATCMPSourcepoint(accountId: "accountId", propertyId: "propertyId", propertyName: "propertyName", pmId: "pmId") 
    // [CONSENT] AATKit also has AATCMPOgury. Please check the wiki at: https://aatkit.gitbook.io/ios-integration/start/consent/general-handling
    var window: UIWindow?

    //MARK:- Application life cycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //MARK:- AATKit init work
        let configuration = AATConfiguration()

        //MARK:- [CONSENT] AATManagedConsent Work
        // [CONSENT] You have to conform to AATManagedConsentDelegate to work with the AATManagedConsent
        let consent = AATManagedConsent.init(cmp: cmp, delegate: self)
        
        configuration.consent = consent

        // !IMPORTANT! this line for this demo purpose only and shouldn't be used in live apps
        configuration.testModeAccountId = 1995

        AATSDK.initAATKit(with: configuration)
        AATSDK.setLogLevel(logLevel: .debug)
        
        
        return true
    }

}

//MARK:- AATManagedConsentDelegate
extension AppDelegate: AATManagedConsentDelegate {
    func managedConsentCMPFinished(with state: AATManagedConsentState) {
        print("AATManagedConsent finished with state: \(state)")
        switch state {
        case .unknown, .withheld:
            print("User didn't give consent")
        case .custom, .obtained:
            // [ATT] you might need to display the iOS tracking popup here (ATT)
            print("User gave consent")
            if #available(iOS 14.5, *) {
                ATTrackingManager.requestTrackingAuthorization { [weak self] status in
                    print(status)
                    self?.consentFinished()
                }
            }
        default:
            print("Unknown state!")
        }
    }
    
    func managedConsentNeedsUserInterface(_ managedConsent: AATManagedConsent) {
        // [CONSENT] get your current top UIViewController (depending on your viewController hierarchy) and pass it to the managedConsent to display the CMP
        guard let vc = UIApplication.shared.windows.first?.rootViewController else {
            return
        }
        managedConsent.showIfNeeded(vc)
    }
    
    func managedConsentCMPFailedToLoad(_ managedConsent: AATManagedConsent, with error: String) {
        print("AATManagedConsent failed to load with error: \(error)")
        consentFinished()
    }
    
    func managedConsentCMPFailedToShow(_ managedConsent: AATManagedConsent, with error: String) {
        print("AATManagedConsent failed to show with error: \(error)")
        consentFinished()
    }
    
    func consentFinished() {
        // [CONSENT] Since this method might not be called from main thread (ATTrackingManager.requestTrackingAuthorization)
        // We have to post this notification from main thread to create the placement from the main thread
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name(NotificationsNames.aatReadyToRequestAds.rawValue), object: nil)
        }
    }
}
