//
//  ViewController.swift
//  AATManagedConsentSample
//
//  Created by Mahmoud Amer on 04.06.21.
//

import UIKit
import AATKit

class ViewController: UIViewController {

    /// Outlets
    @IBOutlet weak var stickyBannerView: UIView!

    /// AATKit
    var stickyBannerPlacement: AATStickyBannerPlacement?

    override func viewDidLoad() {
        super.viewDidLoad()
        stickyBannerPlacement = AATSDK.createStickyBannerPlacement(name: "placement", size: .banner320x53)
        stickyBannerPlacement?.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AATSDK.controllerViewDidAppear(controller: self)
        stickyBannerPlacement?.startAutoReload()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AATSDK.controllerViewWillDisappear()
    }

    func displayStickyBanner(bannerView: UIView) {
        stickyBannerView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        let frame = CGRect(x: 0, y: stickyBannerView.frame.height - 52, width: view.frame.width, height: 52)
        bannerView.frame = frame
        stickyBannerView.addSubview(bannerView)
    }

}

extension ViewController: AATStickyBannerPlacementDelegate {
    func aatPauseForAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatHaveAd(placement: AATPlacement) {
        if let bannerView = stickyBannerPlacement?.getPlacementView() {
            displayStickyBanner(bannerView: bannerView)
        }
    }
    
    func aatNoAd(placement: AATPlacement) {
        print(#function)
    }
}
