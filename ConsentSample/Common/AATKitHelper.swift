//
//  AATKitHelper.swift
//  ConsentSample
//
//  Created by Mahmoud Amer on 04.06.21.
//

import Foundation
import UIKit

enum NotificationsNames: String {
    case aatKitHaveAd
    case aatKitUserEarnedIncentive
    case aatKitNoAds
    case aatReadyToRequestAds
}
