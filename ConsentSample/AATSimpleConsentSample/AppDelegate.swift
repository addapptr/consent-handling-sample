//
//  AppDelegate.swift
//  AATSimpleConsentSample
//
//  Created by Mahmoud Amer on 04.06.21.
//

import UIKit
import AATKit
import AppTrackingTransparency

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK:- Properties
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        let configuration = AATConfiguration()

        //MARK:- [CONSENT] work
        // [CONSENT] You have to pass this value depending on the consent you got from the user
        let consent = AATSimpleConsent(nonIABConsent: .obtained)
        configuration.consent = consent

        // !IMPORTANT! this line for this demo purpose only and shouldn't be used in live apps
        configuration.testModeAccountId = 1995

        AATSDK.initAATKit(with: configuration)
        AATSDK.setLogLevel(logLevel: .debug)

        if #available(iOS 14.5, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                print(status)
            }
        }
        
        return true
    }

}
