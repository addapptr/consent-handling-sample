//
//  AppDelegate.swift
//  ConsentSample
//
//  Created by Mahmoud Amer on 04.06.21.
//

import UIKit
import AATKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK:- Properties
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let configuration = AATConfiguration()

        // !IMPORTANT! this line for this demo purpose only and shouldn't be used in live apps
        configuration.testModeAccountId = 1995

        AATSDK.initAATKit(with: configuration)
        AATSDK.setLogLevel(logLevel: .debug)

        return true
    }

}
