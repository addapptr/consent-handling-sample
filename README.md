# Consent Sample iOS

This app demonstrates different types of AATKit consent.
See also the [wiki](https://bitbucket.org/addapptr/consent-handling-sample/wiki/Home) for more instrucions.

---
## Installation
Clone this repository and import into **Xcode**
```bash
git clone git@bitbucket.org:addapptr/consent-handling-sample.git
```
---
## Maintainers
Current maintainers:

* [Mahmoud Amer](https://bitbucket.org/mahmoudamer89/)
* [Mohamed Matloub](https://bitbucket.org/MMatloub/)
